# Apache Kakfa K8s

This repository helps with the evaluation of running Apache Kafka and Apache Zookeeper on Kubernetes.

# Disclaimer

This project is worked on in my free time and I will not provide support or timely maintenance.
However, please feel free to contribute or submit issues.

# Setup

These tools are not all mandatory in order to evaluate the Apache Kafka and Apache Zookeeper. All that is recommeded is the 
`Kustomize` and `kubectl` cli tools. In order to setup the optional tools a simple script was created to
install the tools and to create a new kubernetes cluster within `docker`. To use the optional tools, you are 
required to install `docker` on you own machine but the scripts shall intall the remainder.

In you favourite terminal:

```bash
bash <(curl -s https://gitlab.com/nystrome/kind-tools/-/raw/main/install.sh)
```

For more info see https://gitlab.com/nystrome/kind-tools

The container images are based on bitmani images for kafka and zookeeper.
To configure your kafka cluster see [docs](https://registry.hub.docker.com/r/bitnami/kafka) for the specific image tag.
To configure you zookeeper see [docs](https://registry.hub.docker.com/r/bitnami/zookeeper) for the specific image tage.
Both are configured via environment variables that are well documented.

# Applying to Kubernetes cluster

The `kustomization.yaml` file at the root of each folder defines what manifests and templates will be applied when creating a unified yaml manifest.
The `zookeeper` pod is required for kafka pods to function. 

To delete test cluster see `tools` folder README.

# Testing

The kafka can be accessed within kubernetes by pointing your services to the dns entry `kafka.kafka`.
In the setup there is the kafkacat cli. This allows you to read and write to and from kafka topics. More info on the tool [here](https://docs.confluent.io/platform/current/app-development/kafkacat-usage.html).

## Producing to kafka

Using kafkacat in producer mode you can write messages to kafka topics.
More info [here](https://docs.confluent.io/platform/current/app-development/kafkacat-usage.html#producer-mode)
First login to the kafkacat pod.

```bash
 kubectl exec -it kafkacat -- sh

 kafkacat -b kafka:9092 -t new_topic -P
```

## Consuming from kafka

Using kafkacat in producer mode you can write messages to kafka topics.
More info [here](https://docs.confluent.io/platform/current/app-development/kafkacat-usage.html#consumer-mode)
First login to the kafkacat pod.

```bash
 kubectl exec -it kafkacat -- sh

 kafkacat -b kafka:9092 -t new_topic -C
```

# Maintenance

In order to keep this project going I will from time to time update the versions and other items such as security.
If there are other requests feel free to contribute by forking the project and submitting a merge request.
If you have other suggestions feel free to submit an issue. I will have a look at it at when I have the free time.

## Contributing

Please take a fork of this repository, work on bits and pieces as you like and submit a merge request with your changes.
I only ask that the yaml files are sorted by alphabetical order of its keys. See [VSCode extention](https://marketplace.visualstudio.com/items?itemName=PascalReitermann93.vscode-yaml-sort).

## Raising Issues

Feel free to leave an issue with detailed descriptions of any problems that you have faced with getting your Kubernetes 
cluster created or kafka/zookeeper installed. Summit images in your issue requests or add in logging information from
affected pod as needed.

